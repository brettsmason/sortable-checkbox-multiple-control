<?php
/**
 * The sortable multiple checkbox customize control allows theme authors to add theme options that have
 * multiple choices and save the order.
 */
/**
 * Multiple checkbox customize control class.
 *
 * @access public
 */
class BM_Customize_Control_Sortable_Checkbox_Multiple extends WP_Customize_Control {
	/**
	 * The type of customize control being rendered.
	 *
	 * @access public
	 * @var    string
	 */
	public $type = 'sortable-checkbox-multiple';
	/**
	 * Enqueue scripts/styles.
	 *
	 * @access public
	 * @return void
	 */
	public function enqueue() {
		$control_dir = trailingslashit( get_template_directory_uri() ) . 'includes/customizer/controls/control-sortable-checkbox-multiple';
		wp_enqueue_script( 'jquery-ui-core' );
		wp_enqueue_script( 'jquery-ui-sortable' );
		wp_enqueue_style( 'control-sortable-checkbox-multiple', $control_dir . '/control-sortable-checkbox-multiple.css' );
		wp_enqueue_script( 'control-sortable-checkbox-multiple', $control_dir . '/control-sortable-checkbox-multiple.js', array('jquery', 'jquery-ui-sortable'), '', true );
	}
	/**
	 * Add custom parameters to pass to the JS via JSON.
	 *
	 * @access public
	 * @return void
	 */
	public function to_json() {
		
		parent::to_json();
		$this->json['value']   = !is_array( $this->value() ) ? explode( ',', $this->value() ) : $this->value();
		$this->json['choices'] = $this->choices;
		$this->json['link']    = $this->get_link();
		$this->json['id']      = $this->id;
	}
	/**
	 * Underscore JS template to handle the control's output.
	 *
	 * @access public
	 * @return void
	 */
	public function content_template() { ?>

		<# if ( ! data.choices ) {
			return;
		} #>

		<# if ( data.label ) { #>
			<span class="customize-control-title">{{ data.label }}</span>
		<# } #>

		<# if ( data.description ) { #>
			<span class="description customize-control-description">{{{ data.description }}}</span>
		<# } #>

		<ul>
			<# _.each( data.choices, function( label, choice ) { #>
				<li>
					<label>
						<input type="checkbox" value="{{ choice }}" <# if ( -1 !== data.value.indexOf( choice ) ) { #> checked="checked" <# } #> />
						{{ label }}
					</label>
					<i class='dashicons dashicons-menu'></i>
				</li>
			<# } ) #>
		</ul>
	<?php }
}