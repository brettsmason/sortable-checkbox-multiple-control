( function( $, api ) {

    /* === Checkbox Multiple Control === */

    api.controlConstructor['sortable-checkbox-multiple'] = api.Control.extend( {
        ready: function() {
            var control = this;

            $( '.customize-control-sortable-checkbox-multiple > ul' ).sortable({
                handle: '.dashicons-menu',
                axis: 'y',
            });

            $( 'input:checkbox', control.container ).change(
                function() {

                    // Get all of the checkbox values.
                    var checkbox_values = $( 'input[type="checkbox"]:checked', control.container ).map(
                        function() {
                            return this.value;
                        }
                    ).get();

                    // Set the value.
                    if ( null === checkbox_values ) {
                        control.setting.set( '' );
                    } else {
                        control.setting.set( checkbox_values );
                    }
                }
            );
        }
    } );

} )( jQuery, wp.customize );