<?php
/* Theme Customizer setup. */
add_action( 'customize_register', 'themeslug_customize_register' );

/**
 * Sets up the theme customizer sections, controls, and settings.
 */
function themeslug_customize_register( $wp_customize ) {
	
	require_once trailingslashit( get_template_directory() ) . 'includes/customizer/controls/control-sortable-checkbox-multiple/control-sortable-checkbox-multiple.php';

	$wp_customize->register_control_type( 'BM_Customize_Control_Sortable_Checkbox_Multiple' );

	$wp_customize->add_setting( 'template_layout',
		array(
			'default' => array('title', 'byline', 'image')
		)
	);

    $wp_customize->add_control(
        new BM_Customize_Control_Sortable_Checkbox_Multiple(
            $wp_customize,
            'template_layout',
            array(
                'section' => 'title_tagline',
                'label'   => __( 'Template Layout', 'textdomain' ),
                'choices' => array(
                    'title' => __( 'Title', 'textdomain' ),
                    'byline' => __( 'Byline', 'textdomain' ),
                    'image' => __( 'Image', 'textdomain' )
                )
            )
        )
    );
}